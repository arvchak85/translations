require 'nokogiri'
require "keyvalue.rb"
class TranslationsController < ApplicationController
  
  
  def create
    
    @string = "file"
   
    
    
  end  
  def update
    
    @string = "file"
   
    
    
  end  
  
  def new 
    
    uploaded_io = params[:file][:xml]
    File.open(Rails.root.join('public', 'value', uploaded_io.original_filename), 'wb') do|file|
        file.write(uploaded_io.read)
    end
    File.open(Rails.root.join('public', 'value', "strings_id.xml"), 'wb') do|file|
        file.write("")
    end
    File.open(Rails.root.join('public', 'value', "strings_th.xml"), 'wb') do|file|
        file.write("")
    end
  end    
  
  def replace 
    uploaded_io = params[:file][:xml]
    File.open(Rails.root.join('public', 'value', uploaded_io.original_filename), 'wb') do|file|
        file.write(uploaded_io.read)
    end
  end    
  
  def ID
    file = File.join(Rails.root, 'public', 'value', 'strings.xml')
    fileId = File.join(Rails.root, 'public', 'value', 'strings_id.xml')
      @doc = File.open(file) { |f| Nokogiri::XML(f) }
      @docx = File.open(fileId) { |f| Nokogiri::XML(f) }
      $stringValues = []
      $destString = []
       i=0
       @doc.xpath('//string').each do|link|
           vals = KeyValue.new
           vals.setId(link.get_attribute('name'))
           vals.set_format(link.get_attribute('formatted'))
           vals.set_name(link.content)
           $stringValues <<  vals
           $destString  << vals
       end
       @doc.xpath('//drawable').each do|link|
           vals = KeyValue.new
           vals.setId(link.get_attribute('name'))
           vals.set_format(link.get_attribute('formatted'))
           vals.set_name("drawable")
           vals.set_value(link.content)
           $stringValues.push(vals)
           $destString.push(vals)
       end
       
       @docx.xpath('//string').each do|link|
           vals = KeyValue.new
           vals.setId(link.get_attribute('name'))
           vals.set_value(link.content)
           $destString.each do |myString|
               if($destString[i].id.eql?vals.id)
                   $destString[i].set_value(vals.value)
               end  
               i=i+1
           end
      i = 0 
       
      end
  end
  
  def th
    file = File.join(Rails.root, 'public', 'value', 'strings.xml')
    fileId = File.join(Rails.root, 'public', 'value', 'strings_th.xml')
      @doc = File.open(file) { |f| Nokogiri::XML(f) }
      @docx = File.open(fileId) { |f| Nokogiri::XML(f) }
      $stringValues = []
      $destString = []
       i=0
      @doc.xpath('//string').each do|link|
          vals = KeyValue.new
          vals.setId(link.get_attribute('name'))
          vals.set_format(link.get_attribute('formatted'))
          vals.set_name(link.content)
          $stringValues <<  vals
          $destString  << vals
      end
      @doc.xpath('//drawable').each do|link|
          vals = KeyValue.new
          vals.setId(link.get_attribute('name'))
          vals.set_format(link.get_attribute('formatted'))
          vals.set_name("drawable")
          vals.set_value(link.content)
          $stringValues.push(vals)
          $destString.push(vals)
      end
      
      @docx.xpath('//string').each do|link|
          vals = KeyValue.new
          vals.setId(link.get_attribute('name'))
          vals.set_value(link.content)
          $destString.each do |myString|
          if($destString[i].id.eql?vals.id)
            $destString[i].set_value(vals.value)
          end  
          i=i+1
      end
      i = 0 
       
      end
  end
  
  def generate_xml
    i = 0 
   fileId = File.join(Rails.root, 'public', 'value', 'strings_id.xml')
   @docx = File.open(fileId , "w") 
      $destString.each do|link|
         link.value = params["name"][i]
         link.formatted = params["formatted"][i]
         i = i+1  
      end 
    
       @doc = Nokogiri::XML::Builder.new {|xml|
         xml.resources{
       $destString.each do|link|
           if link.name==="drawable" then
               xml.drawable("name"=>link.id){
                   xml.text(link.value)
               }
               elsif link.formatted === "" then
               xml.string("name"=>link.id){
                   xml.text(link.value)
               }
               elsif link.formatted === "false" then
               xml.string("name"=>link.id, "formatted"=> link.formatted){
                   xml.text(link.value)
               }
               end
      end
    }
      }
       @docx.puts @doc.to_xml
       @docx.close
       
  end   
  
  
  def generate_xml_th
    i = 0 
   fileId = File.join(Rails.root, 'public', 'value', 'strings_th.xml')
   @docx = File.open(fileId , "w:UTF-8")
      $destString.each do|link|
         link.value = params["name"][i]
         link.formatted = params["formatted"][i]
         i = i+1  
      end 
    
       @doc = Nokogiri::XML::Builder.new {|xml|
         xml.resources{
       $destString.each do|link|
           if link.name==="drawable" then
               xml.drawable("name"=>link.id){
                   xml.text(link.value)
               }
           elsif link.formatted === "" then
           xml.string("name"=>link.id){
             xml.text(link.value)
           }
           elsif link.formatted === "false" then
           xml.string("name"=>link.id, "formatted"=> link.formatted){
               xml.text(link.value)
           }
           end
      end
    }
      }
       @docx.puts @doc.to_xml(:encoding => 'utf-8')
       @docx.close
       
  end   
  
  
  
end
